#include <iostream>

using namespace std;

#define SIZE 26
#define M 4
#define N 4

#define chartoint(c) ((int)c - (int)'A')

struct TrieNode
{
	TrieNode *children[SIZE];
	bool isLeafNode;
};

TrieNode* createNode() {
	TrieNode* newNode = new TrieNode();
	for (int i = 0; i < SIZE; ++i)
	{
		newNode->children[i] = NULL;
	}
	newNode->isLeafNode = false;
	return newNode;
}

void insert(TrieNode* root, string word) {
	int n = word.length();
	TrieNode *temp = root;
	
	for (int i = 0; i < n; ++i)
	{
		int index = chartoint(word[i]);
		if(temp->children[index] == NULL)
			temp->children[index] = createNode();
		temp = temp->children[index];
	}
	temp->isLeafNode = true;
}

bool isSafe(int i, int j, bool visited[M][N]) {
	return (i >=0 && i < M && j >=0 &&
			j < N && !visited[i][j]);
}

void searchTrie(TrieNode *root, char boggle[M][N], int i, int j, bool isVisited[M][N], string word) {
	if (root->isLeafNode == true)
		cout << word << endl;

	if (isSafe(i, j, isVisited))
	{
		isVisited[i][j] = true;
		for (int K = 0; K < SIZE; ++K)
		{
			if (root->children[K] != NULL)
			{
				char ch = (char)K + (char)'A' ;

				if (isSafe(i+1, j+1, isVisited) && boggle[i+1][j+1] == ch)
					searchTrie(root->children[K], boggle, i+1, j+1, isVisited, word+ch);
				if (isSafe(i, j+1, isVisited) && boggle[i][j+1] == ch)
					searchTrie(root->children[K], boggle, i, j+1, isVisited, word+ch);
				if (isSafe(i-1, j+1, isVisited) && boggle[i-1][j+1] == ch)
					searchTrie(root->children[K], boggle, i-1, j+1, isVisited, word+ch);
				if (isSafe(i+1, j, isVisited) && boggle[i+1][j] == ch)
					searchTrie(root->children[K], boggle,i+1, j, isVisited, word+ch);
				if (isSafe(i+1, j-1, isVisited) && boggle[i+1][j-1] == ch)
					searchTrie(root->children[K], boggle, i+1, j-1, isVisited, word+ch);
				if (isSafe(i, j-1, isVisited) && boggle[i][j-1] == ch)
					searchTrie(root->children[K], boggle, i, j-1, isVisited, word+ch);
				if (isSafe(i-1, j-1, isVisited) && boggle[i-1][j-1] == ch)
					searchTrie(root->children[K], boggle, i-1, j-1, isVisited, word+ch);
				if (isSafe(i-1, j, isVisited) && boggle[i-1][j] == ch)
					searchTrie(root->children[K], boggle, i-1, j, isVisited, word+ch);
			}
		}

		isVisited[i][j] = false;
	}
}

void findWords(char boggle[M][N], TrieNode *root)
{
	bool isVisited[M][N];
	memset(isVisited, false, sizeof(isVisited));

	TrieNode *temp = root ;

	string word = "";

	for (int i = 0 ; i < M; i++)
	{
		for (int j = 0 ; j < N ; j++)
		{
			if ( temp->children[chartoint(boggle[i][j])] )
			{
				word = word + boggle[i][j];
				searchTrie(temp->children[chartoint(boggle[i][j])], boggle, i, j, isVisited, word);
				word = "";
			}
		}
	}
}

int main() {
	string dictionary[] = {
		"BOT", "BOGGLE", "BIG", "OIL", "TOIL", "BOIL", "LIFE",
		"LIT", "GOT", "NEAR", "SIT", "SLOT", "SIGN", "LIE", 
		"LION", "LIAR", "GOAL", "SEAL", "LIGHT", "LINE", "STONE",
		"SLOB", "FEAR", "FAGGOT", "EAR", "REAL", "FERAL", "LEAF"
	};

	TrieNode* root = createNode();
	int n = sizeof(dictionary)/sizeof(dictionary[0]);
	for (int i = 0; i < n; ++i)
	{
		insert(root, dictionary[i]);
	}

	char boggle[M][N] = {
		{'B','O','T','Q'},
		{'L','I','G','N'},
		{'S','F','E','G'},
		{'H','R','A','L'}
	};

	findWords(boggle, root);

	return 0;
}